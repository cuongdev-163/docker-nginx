FROM nginx:1.10

COPY . /var/www

COPY ./nginx/app.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
